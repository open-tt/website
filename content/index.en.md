# Introduction

![Logo of OpenTT – Racket with Cat](images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

!!! tip ""
	Open Documents and Applications for Table Tennis.

This website provides applications, documents, and information all around table tennis.
The focus is on refereeing for now, this will be extended if needed.

- [Racket Control](/racket-control/) – racket control for all made easy

All data, content, and documents are in gitlab, this is also the place for suggestions, bugs or improvements:

- https://gitlab.com/opentt – gitlab group
- https://gitlab.com/groups/opentt/-/issues – issue tracker (collector of the group)
