# Questions and Answers

![Logo of OpenTT – Racket with Cat](images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

???+ question "Who?"

	I'm Ekkart Kleinod, Berlin's association referee chairman and table tennis referee.

	I work as a research assistant at Fraunhofer FOKUS, play table tennis in Berlin (sometimes in higher leagues, sometimes lower), have two cats, several bicycles and like programming.

	You can find me (among others) here:

	- https://www.tt-schiri.de
	- https://gitlab.com/ekleinod
	- https://twitter.com/edgesoft
	- https://www.edgesoft.de

??? question "Why?"

	During refereeing I rather urgently lacked two things:

	- a reasonable referee management application
	- a LARC app

	Since the referee management was more urgent, I started programming it.

	Meanwhile, the LARC is online and the PDF has taken on a double-digit page count, so this problem became more urgent, too.
	The ITTF does not respond to inquiries about this, so I simply started myself.

??? question "Why open source?"

	I develop the referee stuff on a voluntary basis, just like my job as referee or umpire.

	For programming I use open software - Linux, Java, Android, ...

	For these reasons it was no question for me to publish the results and to use an open source license for it.
	This way many can benefit from it, be it the programs, parts of them, the ideas, or documents.

	In addition I hope that in the long run know-how will be created for the TT community.

??? question "Why a .de domain?"

	Because it is cheaper and the whole thing was initially planned in German.

	If things pick up speed, an org-address can still be organized, if that is necessary.

	Important: start early, then improve.

??? question "Why a cat in the logo?"

	Because cats are cute.

??? question "Join in?"

	Sure, everyone can benefit from that.

	I just don't have an organized plan yet, where it makes sense to participate.

	So everything is open:

	- programming
	- translation
	- proofreading
	- suggestions for improvement
	- error messages
	- new design

	Everything you can think of and find - bring it to us, then we will sort what will be implemented and how.

	First point of contact should be the issue trackers of the projects.
	In the long run I will create specific issues I need help with, these will be labeled "Help Needed".

	- https://gitlab.com/opentt/
	- [Help Needed](https://gitlab.com/groups/opentt/-/issues?label_name%5B%5D=Status%3A+Help+Needed)

	Open the issue collector of `opentt` or directly look in the projects.

	You can also send me an email, please use the prefix `[opentt]` in front of the subject, so I can sort it quickly :smile:

	- <ekleinod@edgesoft.de>
