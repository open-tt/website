# Fragen und Antworten

![Logo von OpenTT – Schläger mit Katze](images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

???+ question "Wer?"

	Ich bin Ekkart Kleinod, Verbandsschiedsrichterobmann von Berlin und Tischtennis-Schiedsrichter.

	Ich arbeite als Wissenschaftlicher Mitarbeiter bei Fraunhofer FOKUS, spiele Tischtennis in der Berliner Bezirksklasse (mal höher, mal niedriger), habe zwei Katzen, mehrere Fahrräder und programmiere gern.

	Ihr findet mich (u.a.) hier:

	- https://www.tt-schiri.de
	- https://gitlab.com/ekleinod
	- https://twitter.com/edgesoft
	- https://www.edgesoft.de

??? question "Warum?"

	In meiner Schiriarbeit fehlten mir eher dringend zwei Dinge:

	- eine vernünftige Schiriverwaltung
	- eine LARC-App

	Da die Verwaltung dringender war, habe ich mit deren Programmierung begonnen.

	Seit die LARC online ist und das PDF eine zweistellige Seitenzahl angenommen hat, wurde auch dieses Problem dringender.
	Die ITTF antwortet auf Anfragen diesbezügich nicht, also habe ich mich erst einmal aufgemacht.

??? question "Warum Open Source?"

	Ich programmiere die Schirisachen ehrenamtlich, genau wie meine Tätigkeit als Schiri oder Obmann.

	Für die Programmierung benutze ich offene Software – Linux, Java, Android, ...

	Aus diesen Gründen war es für mich keine Frage, die Ergebnisse zu veröffentlichen und dafür eine Open-Source-Lizenz zu verwenden.
	So können viele davon profitieren, seien es die Programme, Teile davon, die Ideen oder Dokumente.

	Außerdem hoffe ich, dass langfristig Know-How für die TT-Community entsteht.

??? question "Warum eine .de-Domain?"

	Weil es billiger ist und die ganze Sache zunächst in Deutsch geplant war.

	Falls das Ganze Fahrt aufnimmt, kann immer noch eine org-Adresse organisiert werden, falls das nötig ist.

	Wichtig ist erst mal: anfangen, dann verbessern.

??? question "Warum eine Katze im Logo?"

	Weil Katzen niedlich sind.

??? question "Mitmachen?"

	Gern, davon haben alle was.

	Ich hab bloß derzeit noch keinen geordneten Plan, wo Mitmachen sinnvoll ist.

	Also ist alles offen:

	- Programmierung
	- Übersetzung
	- Korrekturlesen
	- Verbesserungsvorschläge
	- Fehlermeldungen
	- neues Design

	Alles, was Euch einfällt und zu finden ist – her damit, dann sortieren wir, was wie umgesetzt wird.

	Erste Anlaufstelle sollten die Issue-Tracker der Projekte sein.
	Langfristig werde ich gezielt Issues erstellen, bei denen ich Hilfe benötige, diese sind dann mit "Help Needed" gelabelt.

	- https://gitlab.com/opentt/
	- [Help Needed](https://gitlab.com/groups/opentt/-/issues?label_name%5B%5D=Status%3A+Help+Needed)

	Direkt den Issue-Sammler von `opentt` aufmachen oder in den Projekten schauen.

	Ihr könnt mir auch eine E-Mail schicken, dann bitte mit dem Präfix `[opentt]` vor dem Subject, damit ich das schnell zuordnen kann :smile:

	- <ekleinod@edgesoft.de>
