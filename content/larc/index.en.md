# LARC

![Logo of OpenTT – Racket with Cat](../images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

!!! tip ""
	Unofficial list of authorized racket coverings.

Without much ado:

- [API Documentation incl. OpenAPI spec](/larc/api/)
- [API sample client](/larc/api-client/)

The ITTF's LARC was initially a PDF list, now it's an [online list](https://equipments.ittf.com/#/equipments/racket_coverings) with PDF export.
While this is already an improvement, here's what's missing:

- REST API for a LARC app
- compact PDF for two years now

This bothered me for several years, the ITTF does not respond to emails about this.
So this year I took matters into my own hands and set about creating my own API and app.

This is a lot to take on at once, so the plan is as follows:

1. API definition, which will be improved incrementally
2. online API client as PoC and test of the API
3. website
4. android app

The first three items are in progress, the fourth will come with the consolidation of the API and after I build up appropriate know-how.
