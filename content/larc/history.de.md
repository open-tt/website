# Geschichte

![Logo von OpenTT – Schläger mit Katze](../images/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

!!! remark "Anmerkung"
	In Klammern gebe ich meine eigenen Listen-IDs als Referenz an und verlinke das PDF.

Ich weiß nicht genau, wann die erste LARC veröffentlicht wurde und wie genau.
Mein [Archiv](https://www.tt-schiri.de/einsatzinformationen/belaglisten/) beginnt mit Liste 24B ([2004-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-1.pdf)), der zweiten Liste der Saison 2003.
Wenn das jemand ergänzen kann – her mit den Listen :smile:

Jede Liste wurde bis Liste 2020A ([2020-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-1.pdf)) als PDF-Datei im Internet veröffentlicht und den Schiris zur Verfügung gestellt.
Seit Oktober 2020 ([2020-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-2.pdf)) steht die Liste im Internet als HTML-Tabelle mit Filtern, Sortierfunktion sowie PDF- und Excel-Export zur Verfügung.
War Liste 2020A ([2020-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-1.pdf)) noch 8 Seiten lang, ist die Nachfolgerin mit dem Tabellenexport auf 18 Seiten angewachsen.

In einer LARC sind Hersteller, Name, Zulassungscode, Noppentyp und Farbe erfasst.
Seit Oktober 2021 ([2020-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-2.pdf)) sind andere Farben als rot und schwarz zugelassen und tauchen erstmalig bei den Belägen auf.

## Gültigkeit

Jede Liste hat eine Gültigkeitsdauer, diese war bis Oktober 2020 ([2020-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-2.pdf)) ungefähr ein halbes Jahr, die genauen Daten schwankten.
Mal war eine Liste ab April gültig, mal ab Juni etc.
Außerdem überlappten sich die Gültigkeitsdaten.

Das machte es oft schwierig, die aktuell gültigen Beläge herauszufinden.

Mit LARC 35B ([2014-3](https://www.tt-schiri.de/downloads/belaglisten/LARC_2014-3.pdf)) wurde eingeführt, dass sich LARC nicht mehr überlappen bzw. nur noch die neueste LARC gültig ist.

Im April 2021 ([2021-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2021-1.pdf)) wurde auf eine vierteljährliche Gültigkeit umgestellt.

Die in einem Verband zugelassenen Beläge richten sich nach den Bestimmungen des Verbandes.

Bei ITTF-Veranstaltungen sind nur die Beläge der aktuellen LARC zugelassen.
Im Bereich des DTTB gelten alle Beläge als gültig, die bei Beginn der Saison gültig waren, das heißt mit fortschreitender Saison sind Beläge mehrerer Listen zugelassen.

## Benennung

Die Benennung der Listen durch die ITTF ist sehr uneinheitlich und sehr kompliziert, daher habe ich für meine Zwecke ein eigenes [Namensschema](../naming) entwickelt.

Die Benennung der ITTF ist wie folgt:

Liste 24B ([2004-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-1.pdf)) und folgende wurden numeriert, der Buchstabe gibt an, ob die Liste die erste der Saison ist (kein Buchstabe) oder die zweite der Saison (B).
Dann wird die Nummer erhöht.

Liste 24B ([2004-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-1.pdf)) ist also die zweite Liste der Saison 2003, gefolgt von Liste 25 ([2004-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-2.pdf)), der ersten Liste der Saison 2004, wiederum gefolgt von 25B ([2005-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2005-1.pdf)) etc. pp.

- Saison 2003: LARC 24 und 24B
- Saison 2004: LARC 25 und 25B
- ...

LARC 35A ([2014-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2014-2.pdf)) war die einzige LARC, bei der der Buchstabe "A" an die Nummer gehängt wurde, statt der einfachen 35.

2015 wurde die Benennung umgestellt, LARC 35B ([2014-3](https://www.tt-schiri.de/downloads/belaglisten/LARC_2014-3.pdf)) war die letzte LARC nach altem Schema, danach wurden die Listen mit Jahreszahl und Folgebuchstaben benannt.
Die erste Liste 2015 (Nachfolgerin von 35B) hieß daher 2015A ([2015-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2015-1.pdf)).
Die Benennung wechselte also von Saison zu Jahr.

- Jahr 2015: LARC 2015A und 2015B
- Jahr 2016: LARC 2016A und 2016B
- ...

Seit der Umstellung auf eine Online-Tabelle gibt es keine Namen mehr für die Listen.
Die Identifikation erfolgt über die Gültigkeitsdaten.
