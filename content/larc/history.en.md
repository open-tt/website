# History

![Logo of OpenTT – Racket with Cat](../images/icon.png "Logo of OpenTT – Racket with Cat"){ align=right width=100 }

!!! remark "Remark"
	I am providing my own list IDs for reference in parentheses, and link to the PDF.

I don't know exactly when the first LARC was published or how.
My [archive](https://www.tt-schiri.de/einsatzinformationen/belaglisten/) starts with list 24B ([2004-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-1.pdf)), the second list of the 2003 season.
If anyone can add to that – bring on the lists :smile:

Each list was published as a PDF file on the Internet and made available to referees until List 2020A ([2020-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-1.pdf)).
Since October 2020 ([2020-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-2.pdf)) the list is available on the internet as HTML table with filters, sorting function and PDF and Excel export.
While list 2020A ([2020-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-1.pdf)) was 8 pages long, its successor has grown to 18 pages with the table export.

In a LARC, the manufacturer, name, approval code, pimple type and color are recorded.
Since October 2021 ([2020-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-2.pdf)), colors other than red and black are approved and appear for the first time.

## Validity

Each list has a validity period, this was roughly half a year until October 2020 ([2020-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2020-2.pdf)), the exact dates fluctuated.
Sometimes a list was valid from April, sometimes from June, etc.
In addition, the validity dates overlapped.

This often made it difficult to find out which coverings were currently valid.

LARC 35B ([2014-3](https://www.tt-schiri.de/downloads/belaglisten/LARC_2014-3.pdf)) introduced that LARCs no longer overlap or only the latest LARC is valid.

In April 2021 ([2021-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2021-1.pdf)), the change was made to quarterly validity.

The coverings allowed in a federation are based on the regulations of the federation.

At ITTF events only coverings of the current LARC are allowed.
In the area of the DTTB all coverings are considered valid which were valid at the start of the season, i.e. as the season progresses coverings of several lists are allowed.

## Naming

The naming of the lists by the ITTF is very inconsistent and very complicated, so I have developed my own [naming scheme](../naming) for my purposes.

The ITTF naming is as follows:

List 24B ([2004-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-1.pdf)) and following were numbered, the letter indicates whether the list is the first of the season (no letter) or the second of the season (B).
Then the number is incremented.

So list 24B ([2004-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-1.pdf)) is the second list of the 2003 season, followed by list 25 ([2004-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2004-2.pdf)), the first list of the 2004 season, again followed by 25B ([2005-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2005-1.pdf)) etc. pp.

- 2003 season: LARC 24 and 24B
- 2004 season: LARC 25 and 25B
- ...

LARC 35A ([2014-2](https://www.tt-schiri.de/downloads/belaglisten/LARC_2014-2.pdf)) was the only LARC to have the letter "A" appended to the number, rather than the simple 35.

In 2015, the naming was changed, LARC 35B ([2014-3](https://www.tt-schiri.de/downloads/belaglisten/LARC_2014-3.pdf)) was the last LARC under the old scheme, after which lists were named with year and following letter.
Thus, the first 2015 list (successor to 35B) was named 2015A ([2015-1](https://www.tt-schiri.de/downloads/belaglisten/LARC_2015-1.pdf)).
This means, the naming changed from season to year.

- year 2015: LARC 2015A and 2015B.
- year 2016: LARC 2016A and 2016B
- ...

Since the switch to an online table, there are no longer names for the lists.
The identification is done by the validity dates.
